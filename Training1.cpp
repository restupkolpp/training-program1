﻿#include <iostream>
#include <locale.h>
#include <math.h>
#include <stdlib.h>

using namespace std;
double pi = 3.14;

int GetRandomNumberForDiv(int num)
{
    srand(time(NULL));
    if (num % 3 == 0)
    {
        return (num * (rand() % 10 + 1));
    }
    else if (num % 5 == 0 && num % 10 != 0 && num % 7 != 0)
    {
        return (num * (rand() % 10 + 1));
    }
    else if (num % 10 == 0 && num % 2 != 0)
    {
        return (num * (rand() % 9 + 1));
    }
    else if (num % 7 == 0 && num % 5 != 0) {

        return (num * (rand() % 10 + 1));
    }
    else if (num % 2 == 0)
    {
        return (num * (rand() % 10 + 1));
    }
}

int GetRandomNumber(int min, int max)
{
    // Установить генератор случайных чисел
    srand(time(NULL));

    // Получить случайное число - формула
    int num = min + rand() % (max - min + 1);

    return num;
}

int GetRandomFractionalNumber(double min, double max)
{
    return (double)(rand()) / RAND_MAX * (max - min) + min; // Генератор случайных дробных счисел 
}

long double fact(int N)
{
    return N * fact(N - 1);
}

void First_class()
{
    int c = 0;
    int a;
    int b1, b2;

    cout << "Решите примеры: " << endl;
    for (int i = 0; i < 5; i++)
    {
        b1 = GetRandomNumber(2, 10);
        b2 = GetRandomNumber(1, 9);
        cout << b1 << " + " << b2 << " = ";
        cin >> a;
        if (a == b1 + b2) c++;
        cout << endl;
    }

    for (int i = 0; i < 5; i++)
    {
        b1 = GetRandomNumber(5, 10);
        b2 = GetRandomNumber(1, 5);
        cout << b1 << " - " << b2 << " = ";
        cin >> a;
        if (a == b1 - b2) c++;
        cout << endl;
    }
    cout << "Количество правильно решенных заданий: " << c << endl;
}

void Second_class()
{
    int c = 0;
    int a;
    int b1, b2, b3, b4;
    string a8, a10;

    //1
    b1 = GetRandomNumber(15, 20);
    b2 = GetRandomNumber(0, 15);
    b3 = GetRandomNumber(1, 10);
    cout << "Решите примеры: " << endl << b1 << " - " << b2 << " + " << b3 << " = ";
    cin >> a;
    if (a == b1 - b2 + b3) c++;
    cout << endl;

    //2
    b1 = GetRandomNumber(15, 20);
    b2 = GetRandomNumber(10, 15);
    b3 = GetRandomNumber(1, 10);
    cout << b1 << " - (" << b2 << " - " << b3 << ") = ";
    cin >> a;
    if (a == b1 - (b2 - b3)) c++;
    cout << endl;

    //3
    b1 = GetRandomNumber(1, 10);
    b2 = GetRandomNumber(1, 9);
    b3 = GetRandomNumber(1, 11);
    b4 = GetRandomNumber(1, 12);
    cout << b1 << " + " << b2 << " + " << b3 << " + " << b4 << " = ";
    cin >> a;
    if (a == b1 + b2 + b3 + b4) c++;
    cout << endl;

    //4
    b1 = GetRandomNumber(5, 10);
    b2 = GetRandomNumber(16, 20);
    b3 = GetRandomNumber(1, 5);
    if (b3 == 1)
        cout << "Решите задачу: " << endl << "Через " << b1 << " лет косте будет " << b2 << " лет.\
 Сколько лет было косте " << b3 << " год назад ?" << endl << "Ответ : ";
    if (b3 == 5)
        cout << "Решите задачу: " << endl << "Через " << b1 << " лет косте будет " << b2 << " лет.\
 Сколько лет было косте " << b3 << " лет назад ?" << endl << "Ответ : ";
    else
        cout << "Решите задачу: " << endl << "Через " << b1 << " лет косте будет " << b2 << " лет.\
 Сколько лет было косте " << b3 << " года назад ?" << endl << "Ответ : ";
    cin >> a;
    if (a == b2 - b1 - b3) c++;
    cout << endl;

    //5
    b1 = GetRandomNumber(50, 100);
    b2 = GetRandomNumber(10, 25);
    b3 = GetRandomNumber(19, 25);
    cout << b1 << " - " << b2 << " - " << b3 << " = ";
    cin >> a;
    if (a == b1 - b2 - b3) c++;
    cout << endl;

    //6
    b1 = GetRandomNumber(25, 40);
    b2 = GetRandomNumber(10, 25);
    b3 = GetRandomNumber(1, 10);
    cout << b1 << " - (" << b2 << " - " << b3 << ") =";
    cin >> a;
    if (a == b1 - (b2 - b3)) c++;
    cout << endl;

    //7
    b1 = GetRandomNumber(10, 40);
    b2 = GetRandomNumber(20, 50);
    cout << "Найдите значение выражения a + " << b1 << ", при a = " << b2 << ":" << endl;
    cin >> a;
    if (a == b1 + b2) c++;
    cout << endl;

    //8
    b1 = GetRandomNumber(20, 30);
    b2 = GetRandomNumber(30, 40);
    b3 = GetRandomNumber(20, 30);
    b4 = GetRandomNumber(10, 20);
    cout << "Сравните выражение и поставьте вместо * нужный знак:" << b1 << " + " << b2 << " * " << b3 << " + " << b4 << endl;
    cin >> a8;
    if (a8 == "<" && b1 + b2 < b3 + b4) c++;
    if (a8 == ">" && b1 + b2 > b3 + b4) c++;
    if (a8 == "=" && b1 + b2 == b3 + b4) c++;
    cout << endl;

    //9
    b1 = GetRandomNumber(20, 50);
    b2 = GetRandomNumber(10, 20);
    cout << "В коробке было " << b1 << " кубиков.\
Из нескольких кубиков Костя построил башню, после чего в коробке осталось " << b2 << " кубиков. Сколько кубиков в башне, которую построил Костя ? " << endl << "Ответ : ";
    cin >> a;
    if (a == b1 - b2) c++;
    cout << endl;

    //10
    cout << "Запишите в виде выражения: " << endl << "К 8 прибавить разность чисел 12 и 7" << endl;
    cin >> a10;
    if (a10 == "8 + (12 - 7)" || a10 == "8+(12-7)" || a10 == "8 + (12-7)" || a10 == "8+(12 - 7)") c++;
    cout << endl;

    cout << "Количество правильно решенных заданий: " << c << endl;
}

void Third_class()
{
    int c = 0;
    int a1, a2;
    int b1, b2, b3, b4;

    //1
    b1 = GetRandomNumber(50, 80);
    b2 = GetRandomNumber(15, 45);
    cout << "Найдите значение выражения:" << endl << b1 << " - " << b2 << " = ";
    cin >> a1;
    if (a1 == b1 - b2) c++;
    cout << endl;

    //2
    b1 = GetRandomNumber(300, 600);
    b2 = GetRandomNumber(100, 300);
    cout << b2 << " + " << b1 << " = ";
    cin >> a1;
    if (a1 == b2 + b1) c++;
    cout << endl;

    //3
    b1 = GetRandomNumber(10, 20);
    b2 = GetRandomNumber(2, 9);
    cout << b2 << " * " << b1 << " = ";
    cin >> a1;
    if (a1 == b2 * b1)c++;
    cout << endl;

    //4
    b1 = GetRandomNumberForDiv(18);
    b2 = GetRandomNumberForDiv(3);
    cout << b1 << " : " << b2 << " = ";
    cin >> a1;
    if (a1 == b1 / b2) c++;
    cout << endl;

    //5
    b1 = GetRandomNumber(25, 45);
    b2 = GetRandomNumber(10, 15);
    b3 = GetRandomNumber(46, 60);
    b4 = GetRandomNumber(3, 9);
    cout << b4 << " * (" << b1 << " - " << b2 << ") + (" << b3 << " - " << b1 << ") * " << b4 << " = ";
    cin >> a1;
    if (a1 == b4 * (b1 - b2) + (b3 - b1) * b4) c++;
    cout << endl;

    //6
    cout << "Преобразуй величины:" << endl;
    b1 = GetRandomNumber(1, 7);
    b2 = GetRandomNumber(1, 99);
    cout << "В сантиметры: " << endl;
    cout << "1) " << b1 << " м. " << b2 << " см. = ";
    cin >> a1;
    if (a1 == b1 * 100 + b2) c++;
    cout << endl;

    //7
    b1 = GetRandomNumber(1, 9);
    b2 = GetRandomNumber(1, 99);
    cout << "В копейки: " << endl;
    cout << "2) " << b1 << " р. " << b2 << " к. = ";
    cin >> a1;
    if (a1 == b1 * 100 + b2) c++;
    cout << endl;

    //8
    b1 = GetRandomNumber(1, 20);
    b2 = GetRandomNumber(1, 60);
    cout << "В минуты: " << endl;
    cout << "3) " << b1 << " ч. " << b2 << " мин. = ";
    cin >> a1;
    if (a1 == b1 * 60 + b2) c++;
    cout << endl;

    //9
    b1 = GetRandomNumberForDiv(3);
    cout << "Реши задачу. \
 В школьную столовую привезли " << b1 << " кг яблок, а груш в 3 раза меньше. Сколько всего кг яблок и груш привезли в школьную столовую ? " << endl << "Ответ : ";
    cin >> a1;
    if (a1 == b1 + (b1 / 3.0)) c++;
    cout << endl;

    //10
    b1 = GetRandomNumber(4, 10);
    b2 = GetRandomNumber(2, 11);
    cout << "Ширина прямоугольника " << b1 << " см, а его длина на " << b2 << " см больше. Вычисли периметр и площадь прямоугольника." << endl;
    cout << "Периметр: ";
    cin >> a1;
    cout << "Площадь: ";
    cin >> a2;
    if (a1 == (((b1 + b2) * 2) + (b1 * 2)) && a2 == (b1 + b2) * b1) c++;
    cout << endl;

    cout << "Количество правильно решенных заданий: " << c << endl;
}

void Fourth_class()
{
    int c = 0;
    string a;
    int a1, a2;
    int b1, b2, b3, b4;

    //1
    cout << "Решите примеры: " << endl;
    b1 = GetRandomNumber(9786, 14657);
    b2 = GetRandomNumber(15432, 19689);
    cout << b1 << " + " << b2 << " = ";
    cin >> a1;
    if (a1 == b1 + b2) c++;
    cout << endl;

    //2
    b1 = GetRandomNumber(786576, 698745);
    b2 = GetRandomNumber(154678, 598765);
    cout << b1 << " - " << b2 << " = ";
    cin >> a1;
    if (a1 == b1 - b2) c++;
    cout << endl;

    //3
    b1 = GetRandomNumber(211, 324);
    b2 = GetRandomNumber(3, 7);
    cout << b1 << " * " << b2 << " = ";
    cin >> a1;
    if (a1 == b1 * b2) c++;
    cout << endl;

    //4
    b1 = GetRandomNumber(600084, 600145);
    b2 = GetRandomNumber(597623, 599899);
    b3 = GetRandomNumber(2, 7);
    cout << "(" << b1 << " - " << b2 << ") * " << b3 << " = ";
    cin >> a1;
    if (a1 == (b1 - b2) * b3) c++;
    cout << endl;

    //5
    cout << "Решите уравнения: " << endl;
    b1 = GetRandomNumber(134, 259);
    b2 = GetRandomNumber(567, 654);
    b3 = GetRandomNumber(145, 251);
    cout << b1 << " + " << "x = " << b2 << " - " << b3 << endl << "x = ";
    cin >> a1;
    if (a1 == b2 - b3 - b1) c++;
    cout << endl;

    //6
    b1 = GetRandomNumber(421, 532);
    b2 = GetRandomNumber(221, 345);
    b3 = GetRandomNumber(11, 51);
    cout << b1 << " - " << "x = " << b2 << " + " << b3 << endl << "x = ";
    cin >> a1;
    if (a1 == -1 * (b2 + b3 - b1)) c++;
    cout << endl;

    //7
    b1 = GetRandomNumber(2, 5);
    b2 = GetRandomNumber(6, 10);
    cout << "Найдите площадь прямоугольника со сторонами " << b1 << " см. и " << b2 << " см." << endl;
    cin >> a1;
    if (a1 == b1 * b2) c++;
    cout << endl;

    //8
    b1 = GetRandomNumber(700001, 799453);
    b2 = GetRandomNumber(70001, 79821);
    cout << "Сравните выражения и поставьте вместо * нужный знак: " << b1 << " * " << b2 << endl;
    cin >> a;
    if (a == ">" && b1 > b2) c++;
    cout << endl;

    //9
    b1 = GetRandomNumber(31, 49);
    b2 = GetRandomNumber(2, 9);
    b3 = GetRandomNumber(31, 51);
    b4 = GetRandomNumber(1, 9);
    cout << "Сравните выражения и поставьте вместо * нужный знак: " << b1 << "м. " << b2 << "см." << " * " << b3 << "м. " << b4 << "см." << endl;
    cin >> a;
    if (a == ">" && ((b1 * 100 + b2) > (b3 * 100 + b4))) c++;
    if (a == "<" && ((b1 * 100 + b2) < (b3 * 100 + b4))) c++;
    if (a == "=" && ((b1 * 100 + b2) == (b3 * 100 + b4))) c++;
    cout << endl;

    //10
    b1 = GetRandomNumber(21, 49);
    b2 = GetRandomNumber(23, 54);
    b3 = GetRandomNumber(2, 6);
    if (b3 == 5 || b3 == 6)
    {
        cout << "Из гаража одновременно в противоположных направлениях вышли две машины. \
Одна шла со скоростью " << b1 << " км / ч, другая – со скоростью " << b2 << " км / ч. Какое \
расстояние будет между ними через " << b3 << " часов ? ";
        cin >> a1;
        if (a1 == b1 * b3 + b2 * b3) c++;
    }
    else
    {
        cout << "Из гаража одновременно в противоположных направлениях вышли две машины. \
Одна шла со скоростью " << b1 << " км / ч, другая – со скоростью " << b2 << " км / ч. Какое \
расстояние будет между ними через " << b3 << " часа ? ";
        cin >> a1;
        if (a1 == b1 * b3 + b2 * b3) c++;
    }
    cout << endl;

    cout << "Количество правильно решенных заданий: " << c << endl;
}

void Fifth_class()
{
    int c = 0;
    string a;
    int a1, a2;
    int b1, b2, b3, b4;

    //1
    b1 = GetRandomNumber(15678, 29999);
    b2 = GetRandomNumber(5234, 9999);
    cout << "Какое число на " << b1 << " больше числа " << b2 << "?" << endl;
    cin >> a1;
    if (a1 == b1 + b2) c++;
    cout << endl;

    //2
    b1 = GetRandomNumber(15564, 29896);
    b2 = GetRandomNumber(5264, 9984);
    cout << "На какое число " << b1 << " больше числа " << b2 << "?" << endl;
    cin >> a1;
    if (a1 == b1 - b2) c++;
    cout << endl;

    //3
    b1 = GetRandomNumber(25, 55);
    b2 = GetRandomNumber(17, 27);
    if (b1 % 10 >= 2 && b1 % 10 <= 4)
    {
        if (b2 % 10 >= 2 && b2 % 10 <= 4)
        {
            cout << "В первой коробке " << b1 << " карандаша, что на " << b2 << " карандаша больше, чем во второй. Сколько карандашей во второй коробке?" << endl << "Ответ: ";
            cin >> a1;
            if (a1 == b1 - b2) c++;
        }
        else if ((b2 % 10 >= 5 && b2 % 10 <= 9) || b2 % 10 == 0)
        {
            cout << "В первой коробке " << b1 << " карандаша, что на " << b2 << " карандашей больше, чем во второй. Сколько карандашей во второй коробке?" << endl << "Ответ: ";
            cin >> a1;
            if (a1 == b1 - b2) c++;
        }
        else if (b2 % 10 == 1)
        {
            cout << "В первой коробке " << b1 << " карандаша, что на " << b2 << " карандаш больше, чем во второй. Сколько карандашей во второй коробке?" << endl << "Ответ: ";
            cin >> a1;
            if (a1 == b1 - b2) c++;
        }
    }
    else if ((b1 % 10 >= 5 && b1 % 10 <= 9) || b1 % 10 == 0)
    {
        if (b2 % 10 >= 2 && b2 % 10 <= 4)
        {
            cout << "В первой коробке " << b1 << " карандашей, что на " << b2 << " карандаша больше, чем во второй. Сколько карандашей во второй коробке?" << endl << "Ответ: ";
            cin >> a1;
            if (a1 == b1 - b2) c++;
        }
        else if ((b2 % 10 >= 5 && b2 % 10 <= 9) || b2 % 10 == 0)
        {
            cout << "В первой коробке " << b1 << " карандашей, что на " << b2 << " карандашей больше, чем во второй. Сколько карандашей во второй коробке?" << endl << "Ответ: ";
            cin >> a1;
            if (a1 == b1 - b2) c++;
        }
        else if (b2 % 10 == 1)
        {
            cout << "В первой коробке " << b1 << " карандашей, что на " << b2 << " карандаш больше, чем во второй. Сколько карандашей во второй коробке?" << endl << "Ответ: ";
            cin >> a1;
            if (a1 == b1 - b2) c++;
        }
    }
    else if (b1 % 10 == 1)
    {
        if (b2 % 10 >= 2 && b2 % 10 <= 4)
        {
            cout << "В первой коробке " << b1 << " карандаш, что на " << b2 << " карандаша больше, чем во второй. Сколько карандашей во второй коробке?" << endl << "Ответ: ";
            cin >> a1;
            if (a1 == b1 - b2) c++;
        }
        else if ((b2 % 10 >= 5 && b2 % 10 <= 9) || b2 % 10 == 0)
        {
            cout << "В первой коробке " << b1 << " карандаш, что на " << b2 << " карандашей больше, чем во второй. Сколько карандашей во второй коробке?" << endl << "Ответ: ";
            cin >> a1;
            if (a1 == b1 - b2) c++;
        }
        else if (b2 % 10 == 1)
        {
            cout << "В первой коробке " << b1 << " карандаш, что на " << b2 << " карандаш больше, чем во второй. Сколько карандашей во второй коробке?" << endl << "Ответ: ";
            cin >> a1;
            if (a1 == b1 - b2) c++;
        }
    }
    cout << endl;

    //4
    b1 = GetRandomNumber(695123, 987666);
    b2 = GetRandomNumber(16785, 28765);
    cout << "Вычислить: " << endl << b1 << " - " << b2 << " = ";
    cin >> a1;
    if (a1 == b1 - b2) c++;
    cout << endl;

    //5
    b1 = GetRandomNumber(5465, 11456);
    b2 = GetRandomNumber(1567, 10124);
    b3 = GetRandomNumber(3476, 5678);
    cout << b1 << " + " << b2 << " + " << b3 << " = ";
    cin >> a1;
    if (a1 == b1 + b2 + b3) c++;
    cout << endl;

    //6
    b1 = GetRandomNumber(156, 350);
    b2 = GetRandomNumber(345, 478);
    b3 = GetRandomNumber(15, 29);
    cout << b1 << " - (-x + " << b2 << ")" << " = " << b3 << endl << "x = ";
    cin >> a1;
    if (a1 == (b2 + b3 - b1)) c++;
    cout << endl;

    //7
    b1 = GetRandomNumber(156, 350);
    b2 = GetRandomNumber(345, 478);
    b3 = GetRandomNumber(140, 410);
    b4 = GetRandomNumber(5, 9);
    cout << "Найдите значения выражения " << b1 << " + " << b2 << "х  + " << b3 << "х при х = " << b4 << endl << "Ответ: ";
    cin >> a1;
    if (a1 == b1 + b2 * b4 + b3 * b4) c++;
    cout << endl;

    //8
    b1 = GetRandomNumber(1546, 360);
    b2 = GetRandomNumber(315, 408);
    b3 = GetRandomNumber(130, 350);
    b4 = GetRandomNumber(5, 11);
    cout << "Найдите значения выражения " << b1 << "z + " << b2 << "z  + " << b3 << "z при z = " << b4 << endl << "Ответ: ";
    cin >> a1;
    if (a1 == b1 * b4 + b2 * b4 + b3 * b4) c++;
    cout << endl;

    //9
    b1 = GetRandomNumber(154, 360);
    b2 = GetRandomNumber(315, 408);
    b3 = GetRandomNumber(800, 1125);
    cout << "Света задумала число, прибавила к нему " << b1 << " и к сумме прибавила " << b2 << ". Получила " << b3 << ". Какое число задумала Света?" << endl << "Ответ: ";
    cin >> a1;
    if (a1 == b3 - b1 - b2) c++;
    cout << endl;

    //10
    b1 = GetRandomNumber(1546, 360);
    b2 = GetRandomNumber(315, 408);
    b3 = GetRandomNumber(130, 350);
    b4 = GetRandomNumber(5, 11);
    cout << b1 << " * " << b2 << " + " << b3 << " * " << b4 << " = ";
    cin >> a1;
    if (a1 == b1 * b2 + b3 * b4) c++;
    cout << endl;

    cout << "Количество правильно решенных заданий: " << c << endl;
}

void Sixth_class() {

    srand((unsigned int)time(0));
    int c = 0;
    string a;
    int a1;
    int b1, b2, b3, b4;
    double c1, c2, c3, c4, c5, c6;

    //1
    c1 = GetRandomNumberForDiv(7);
    c2 = GetRandomNumberForDiv(5);
    cout << "Выполните деление " << c1 << " / " << c2 << endl;
    cin >> c3;
    if ((c1 / c2) == c3) c++;
    cout << endl;

    //2-4
    for (int i = 0; i < 3; i++) {
        c1 = floor(GetRandomFractionalNumber(-10, 10)) / 10;
        c2 = floor(GetRandomFractionalNumber(-10, 10)) / 10;
        c3 = floor(GetRandomFractionalNumber(-10, 10)) / 10;
        c4 = floor(GetRandomFractionalNumber(-10, 10)) / 10;
        cout << "Вычислите значение выражения (" << c1 << " * " << c2 << " ) - ( " << c3 << " : " << c4 << " ). При вычислени все значения округляйте до одной десятой." << endl;
        c5 = (floor((c1 * c2) * 10) / 10) - (floor((c3 / c4) * 10) / 10);
        cin >> c6;
        if (c5 == c6) c++;
        cout << endl;
    }

    //5
    b1 = GetRandomNumber(-50, -10);
    b2 = GetRandomNumber(-9, -1);
    cout << "Укажите какое число меньше " << b1 << " " << "или " << b2 << "? Первое соответствует значению - 1, второе - 2." << endl;
    cin >> a1;
    if ((b1 < b2) == a1) c++;
    cout << endl;

    //6
    b1 = GetRandomNumber(5, 15);
    cout << "Радиус круга равен " << b1 << " см. Найдите площадь круга. Ответ округлите до единиц. За число pi взять 3,14 " << endl;
    a1 = round(b1 * b1 * pi) * 10;
    cin >> b2;
    if (b2 == a1) c++;
    cout << endl;

    //7
    b1 = GetRandomNumberForDiv(10);
    b2 = GetRandomNumberForDiv(10);
    b3 = GetRandomNumberForDiv(5);
    b4 = GetRandomNumber(3, 5);
    if (b1 > b2) {
        cout << " Книга, которая стоила " << b2 << " рублей, продаётся с " << b3 << "% - ой скидкой. При покупке " << b4 << " таких книг покупатель отдал кассиру " << b1 << " рублей. Сколько рублей сдачи он должен получить? " << endl;
        cin >> a1;
        if (b1 - ((b2 / 100 * (100 - b3) * b4)) == a1) c++;
    }
    else if (b1 < b2) {
        cin >> a1;
        cout << " Книга, которая стоила " << b1 << " рублей, продаётся с " << b3 << "% - ой скидкой. При покупке " << b4 << " таких книг покупатель отдал кассиру " << b2 << " рублей. Сколько рублей сдачи он должен получить? " << endl;
        if ((b2 - ((b1 / 100 * (100 - b3) * b4)) == a1)) c++;
    }
    else if (b1 == b2) {
        b1 = GetRandomNumberForDiv(10) * GetRandomNumberForDiv(10);
        cout << "Книга, которая стоила " << b2 << " рублей, продаётся с " << b3 << "% - ой скидкой. При покупке " << b4 << " таких книг покупатель отдал кассиру " << b1 << " рублей. Сколько рублей сдачи он должен получить? " << endl;
        cin >> a1;
        if (b1 - ((b2 / 100 * (100 - b3) * b4)) == a1) c++;
    }
    cout << endl;

    //8
    b1 = GetRandomNumber(10, 15);
    b2 = GetRandomNumber(5, 9);
    b3 = GetRandomNumber(3, 5);
    cout << "Сколько понадобится времени " << b1 << " бульдозерам, чтобы расчистить площадку, которую " << b2 << " бульдозеров расчищают за " << b3 << " ч?" << endl;
    cin >> a1;
    if (((b2 * b3) / b3) == a1)c++;
    cout << endl;

    //9
    b1 = GetRandomNumber(900, 1200);
    b2 = GetRandomNumber(600, 800);
    cout << "Свитер стоимостью " << b1 << " руб. продается на распродаже за " << b2 << " руб. На сколько процентов снижена цена свитера?" << endl;
    cin >> c1;
    if (b2 / (b1 / 100) == c1) c++;
    cout << endl;

    //10
    b1 = GetRandomNumberForDiv(2);
    b2 = GetRandomNumberForDiv(10);
    b3 = GetRandomNumberForDiv(5);
    cout << "Решите задачу, составив уравнение. \
Автобус и грузовая машина, скорость которой на " << b1 << " км/ч больше скорости автобуса, выехали одновременно навстречу друг другу из \
двух городов, расстояние между которыми — " << b2 << " км. Определи скорости автобуса и грузовой машины, если известно, что они встретились через " << b3 << " ч. после \
выезда." << endl;
    cin >> a1;
    if (a1 == -1 * (b3 * b1 - b2) / 2) c++;
    cout << endl;

    cout << "Количество правильно решенных заданий: " << c << endl;
}

void Seventh_class() 
{
    int c = 0;
    string a;
    int a1, a2;
    int b1, b2, b3, b4, b5, b6;
    double c1;

    //1
    b1 = GetRandomNumber(2, 9);
    b2 = GetRandomNumber(3, 11);
    b3 = GetRandomNumber(-10, -2);
    cout << "Найдите значения выражения: " << b1 << " - "<< b2 << "x^2 при x = " << b3 << endl << "Ответ: ";
    cin >> a1;
    if (a1 == b1 - b2 * (b3 * b3)) c++;
    cout << endl;

    //2
    b1 = GetRandomNumber(-12, -2);
    b2 = GetRandomNumber(-6, -3);
    cout << "Найдите значения выражения: " << b1 << "p^3 при p = " << b2 << endl << "Ответ: ";
    cin >> a1;
    if (a1 == b1 * (b2 * b2 * b2)) c++;
    cout << endl;

    //3
    b1 = GetRandomNumber(2, 12);
    b2 = GetRandomNumber(3, 13);
    cout << "Выполните действия: " << endl << "c^" << b1 << " * c^" << b2 << endl << "В какой степени должна быть переменная c?" << endl;
    cin >> a1;
    if (a1 == b1 + b2) c++;
    cout << endl;

    //4
    b1 = GetRandomNumber(4, 22);
    b2 = GetRandomNumber(10, 13);
    cout << "Выполните действия: " << endl << "c^" << b1 << " : c^" << b2 << endl << "В какой степени должна быть переменная c?" << endl;
    cin >> a1;
    if (a1 == b1 - b2) c++;
    cout << endl;
    
    //5
    b1 = GetRandomNumber(1, 10);
    b2 = b1 * b1;
    cout << "Решите уравнение: x^2 = " << b1 << endl;
    cout << "x1 = ";
    cin >> a1;
    cout << "x2 = ";
    cin >> a2;
    if ((a1 == b1 && a2 == -1 * b1) || (a1 == -1 * b1 && a2 == b1)) c++;
    cout << endl;

    //6
    cout << "Упростите выражение: " << "5(2a + 1) - 3" << " (Между знаками ставьте пробелы)" << endl;
    cin >> a;
    if (a == "10a + 2" || a == "2 + 10a") c++;
    cout << endl;

    //7
    cout << "Упростите выражение: " << "14x - (x - 1) + (2x + 6)" << " (Между знаками ставьте пробелы)" << endl;
    cin >> a;
    if (a == "15x + 7" || a == "7 + 15x") c++;
    cout << endl;

    //8
    cout << "Упростите выражение: " << "2x - 3y - 11x + 8y" << " (Между знаками ставьте пробелы)" << endl;
    cin >> a;
    if (a == "5y - 9x" || a == "-9x + 5y" || a == "- 9x + 5y") c++;
    cout << endl;

    //9
    b1 = GetRandomNumber(2, 9);
    b2 = GetRandomNumber(-10, 2);
    b3 = GetRandomNumber(10, 15);
    b4 = GetRandomNumber(6, 16);
    b5 = GetRandomNumber(2, 6);
    b6 = GetRandomNumber(5, 15);
    cout << "Найдите значение выражения: " << b1 << "(" << b2 << "a - " << b3 << ") + " << b4 << "a - " << b5 << " при a = " << b6 << endl;
    cin >> a1;
    if (a1 == b1 * (b2 * b6 - b3) + b4 * b6 - b5) c++;
    cout << endl;

    //10
    cout << "Раскройте скобки: 2p - (3p - (2p - c)) (Между знаками ставьте пробелы)" << endl;
    cin >> a;
    if (a == "p - c" || a == "-c + p" || a == "- c + p") c++;
    cout << endl;

    cout << "Количество правильно решенных заданий: " << c << endl;
}

void Eight_class() 
{
    int c = 0;
    string a3;
    int a1, a2;
    int b1, b2, b3, b4, b5, b6, b7, b8;
    double c1, c2, c3, d1, d2;

    //1
    b1 = GetRandomNumber(4, 11);
    b2 = b1 * b1;
    cout << "Найдите значение корня: sqrt(" << b2 << ")" << endl;
    cin >> a1;
    if (a1 == b1) c++;
    cout << endl;

    //2
    b1 = GetRandomNumber(4, 11);
    b2 = b1 * b1;
    cout << "Найдите значение корня: sqrt(" << b2 << ")" << endl;
    cin >> a1;
    if (a1 == b1) c++;
    cout << endl;

    //3
    c1 = floor(GetRandomFractionalNumber(1, 10) * 10) / 100;
    c2 = c1 * c1;
    cout << "Найдите значение корня: sqrt(" << c2 << ")" << endl;
    cin >> d1;
    if (d1 == c1) c++;
    cout << endl;

    //4
    c1 = floor(GetRandomFractionalNumber(1, 10) * 10) / 100;
    c2 = c1 * c1;
    cout << "Найдите значение корня: sqrt(" << c2 << ")" << endl;
    cin >> d2;
    if (d2 == c1) c++;
    cout << endl;
    
    //5
    b1 = GetRandomNumber(4, 11);
    b2 = GetRandomNumber(2, 6);
    b3 = b2 * b2;
    cout << "Найдите значение корня: " << b1 << " * sqrt(" << b3 << ")" << endl;
    cin >> d1;
    if (d1 == b1 * b2) c++;
    cout << endl;

    //6
    b1 = GetRandomNumber(4, 11);
    b2 = GetRandomNumber(5, 12);
    cout << "Найдите значение выражения: sqrt(" << b1 * b1 << " * " << b2 * b2 << ")" << endl;
    cin >> a1;
    if (a1 == b1 * b2) c++;
    cout << endl;

    //7
    b1 = GetRandomNumber(3, 11);
    b2 = GetRandomNumber(4, 12);
    cout << "Найдите значение выражения: sqrt(" << b1 * b1 << " * " << b2 * b2 << ")" << endl;
    cin >> a1;
    if (a1 == b1 * b2) c++;
    cout << endl;

    //8
    b1 = GetRandomNumber(3, 11);
    b2 = GetRandomNumber(4, 12);
    b3 = b1 * b1;
    b4 = b3 - b1;
    cout << "Решите уравнение: x^2 " << " = " << b4 << " + " << b1 << endl << "x1 = ";
    cin >> a1;
    cout << "x2 = ";
    cin >> a2;
    if ((a1 == sqrt(b3) && a2 == -1 * sqrt(b3)) || (a1 == -1 * sqrt(b3) && a2 == sqrt(b3))) c++;
    cout << endl;

    //9
    b1 = GetRandomNumber(4, 14);
    b2 = GetRandomNumber(5, 12);
    b3 = b1 * b1;
    b4 = b3 - b1;
    cout << "Решите уравнение: x^2 - " << b1 << " = " << b4 << endl << "x1 = ";
    cin >> a1;
    cout << "x2 = ";
    cin >> a2;
    if ((a1 == sqrt(b3) && a2 == -1 * sqrt(b3)) || (a1 == -1 * sqrt(b3) && a2 == sqrt(b3))) c++;
    cout << endl;
    
    //10
    b1 = GetRandomNumber(-11, -8);
    b2 = GetRandomNumber(20, 22);
    b3 = GetRandomNumber(17, 19);
    b4 = GetRandomNumber(15, 17);
    b5 = GetRandomNumber(-7, 10);
    b7 = GetRandomNumber(-7, 10);
    b8 = GetRandomNumber(6, 14);
    cout << "Какие из чисел " << b1 << " " << b2 << " " << b3 << " " << b4 << " " << b5 << " " << " принадлежат промежутку [" << b7 << ";" << b8 << "]." << endl;
    cin >> a1;
    if (a1 == b5) c++;
    cout << endl;

    cout << "Количество правильно решенных заданий: " << c << endl;
}

void Ninth_class() 
{
    int c = 0;
    string a5;
    int a1, a2, a3, a4;
    int b1, b2, b3, b4, b5, b6, b7, b8;
    double c1, c2, c3, d1, d2;

    //1
    b1 = GetRandomNumber(2, 10);
    b2 = b1 * b1;
    cout << "Решите уравнение: x^3 - " << b2 << "x = 0" << endl << "x1 = ";
    cin >> a1;
    cout << "x2 = ";
    cin >> a2;
    cout << "x3 = ";
    cin >> a3;
    if ((a1 == 0 && a2 == sqrt(b2) && a3 == -1 * sqrt(b2)) || (a1 == sqrt(b2) && a2 == 0 && a3 == -1 * sqrt(b2)) || (a1 == sqrt(b2) && a2 == -1 * sqrt(b2) && a3 == 0) || (a1 == -1 * sqrt(b2) && a2 == sqrt(b2) && a3 == 0)) c++;
    cout << endl;

    //2
    b1 = GetRandomNumber(3, 13);
    b2 = b1 * b1;
    cout << "Решите уравнение: x^3 - " << b2 << "x = 0" << endl << "x1 = ";
    cin >> a1;
    cout << "x2 = ";
    cin >> a2;
    cout << "x3 = ";
    cin >> a3;
    if ((a1 == 0 && a2 == sqrt(b2) && a3 == -1 * sqrt(b2)) || (a1 == sqrt(b2) && a2 == 0 && a3 == -1 * sqrt(b2)) || (a1 == sqrt(b2) && a2 == -1 * sqrt(b2) && a3 == 0) || (a1 == -1 * sqrt(b2) && a2 == sqrt(b2) && a3 == 0)) c++;
    cout << endl;

    //3
    b1 = GetRandomNumber(-3, 13);
    b2 = GetRandomNumber(-5, 13);
    b3 = GetRandomNumber(-2, 14);
    cout << "Упростите выражение: (a^" << b1 << " * a^" << b2 << ") / a^" << b3 << " В какой степени получится переменная a?" << endl;
    cin >> a1;
    if (a1 == b1 + b2 - b3) c++;
    cout << endl;

    //4
    b1 = GetRandomNumber(-3, 13);
    b2 = GetRandomNumber(-5, 13);
    b3 = GetRandomNumber(-2, 14);
    b4 = GetRandomNumber(-5, 11);
    cout << "Упростите выражение: (a^" << b1 << " * (a^" << b2 << ")^" << b3 << ") / a ^ " << b4 << " В какой степени получится переменная a ? " << endl;
    cin >> a1;
    if (a1 == b1 + b2 * b3 - b4) c++;
    cout << endl;

    //5
    cout << "Сколько шестизначных чисел можно составить из цифр 1, 2, 3, 5, 7, 9 без повторений цифр? " << endl;
    cin >> a1;
    if (a1 == 720) c++;
    cout << endl;

    //6
    b1 = GetRandomNumber(10, 25);
    cout << "Из " << b1 << " туристов надо выбрать дежурного и его помощника. Сколькими способами это можно сделать? " << endl;
    cin >> a1;
    if (a1 == b1 * (b1 - 1)) c++;
    cout << endl;

    //7
    b1 = GetRandomNumber(4, 6);
    cout << "Сколькими способами могут разместиться " << b1 << " человек в салоне автобуса на пяти свободных местах ? " << endl;
    cin >> a1;
    if (a1 == fact(b1)) c++;
    cout << endl;

    //8
    cout << "Победителю конкурса книголюбов разрешается выбрать две книги из 10 различных книг. Сколькими способами он может осуществить этот выбор?" << endl;
    cin >> a1;
    if (a1 == 90) c++;
    cout << endl;

    //9
    cout << "Из 9 книг и 6 журналов надо выбрать 2 книги и 3 журнала. Сколькими способами можно сделать этот выбор? " << endl;
    cin >> a1;
    if (a1 == 720) c++;
    cout << endl;

    //10
    cout << "Решите систему уравнений: " << "{x - y = 2" << endl << "{x * y = 15" << endl;
    cout << "Первая пара корней: (введите значения по очереди)" << endl << "(x1, y1) = ";
    cin >> a1 >> a2;
    cout << "Первая пара корней: (введите значения по очереди)" << endl << "(x2, y2) = ";
    cin >> a3 >> a4;
    if ((a1 == -3 && a2 == -5 && a3 == 5 && a4 == 3) || (a1 == 5 && a2 == 3 && a3 == -3 && a4 == -5)) c++;
    cout << endl;

    cout << "Количество правильно решенных заданий: " << c << endl;
}

void Tenth_class() 
{
    int c = 0;
    int a1, a2, a3, a4;
    int b1, b2, b3, b4, b5, b6, b7, b8;
    double c1, c2, c3, d1, d2;

    //1
    b1 = GetRandomNumber(2, 80);
    cout << "Упростите выражение: (8x / (x^2 - y^2) + " << b1 << " / (x - y) - 4 / (x + y))* (5x - 5y) " << endl << "Ответ: ";
    cin >> a1;
    if (a1 == (b1 + 4) * 5) c++;
    cout << endl;

    //2
    cout << "Решение уравнение (2x + 3) / (x^2 - 2x) - (x - 3) / (x^2 + 2x) = 0" << endl;
    cin >> a1;
    if (a1 == -12) c++;
    cout << endl;

    //3
    b1 = GetRandomNumber(30, 80);
    cout << "Упростите выражение: sin(pi + x) + cos(2pi + x) - sin(-x) - cos(-x) + " << b1 << endl;
    cin >> a1;
    if (a1 == b1) c++;
    cout << endl;

    //4
    b1 = GetRandomNumber(30, 80);
    cout << "Упростите выражение: " << b1 << " + sin(pi - x) + cos(3pi + x) + sin(-x) + cos(-x)" << endl;
    cin >> a1;
    if (a1 == b1) c++;
    cout << endl;

    //5
    cout << "Вычислите: (sin(a) - cos(a))^2 + 2sin(a)*cos(a) " << endl;
    cin >> a1;
    if (a1 == 1) c++;
    cout << endl;

    //6
    cout << "Упростите выражение: cos(a + b) + 2sin(a)*sin(b), если a - b = pi" << endl;
    cin >> a1;
    if (a1 == -1) c++;
    cout << endl;

    //7
    b1 = GetRandomNumber(-2, 10);
    b2 = GetRandomNumber(-3, 9);
    b3 = GetRandomNumber(-4, 10);
    b4 = GetRandomNumber(-5, 12);
    cout << "Упростите выражение: (a^" << b1 << " * a^" << b2 << ")^" << b3 << " / a^" << b4 << " В какой степени будет переменная a?";
    cin >> a1;
    if (a1 == (b1 + b2) * b3 - b4) c++;

    //8
    cout << "Найдите значение выражения (1 / (n^2 - n) - 1 / (n^2 + n)) * (n^2 - 1) / (n + 3) при n = -1" << endl;
    cin >> a1;
    if (a1 == 1) c++;
    cout << endl;

    //9
    cout << "К трехзначному числу приписали цифру 3 сначала справа, потом слева, получились два числа, разность которых равна 3114.\
Найдите это трехзначное число." << endl;
    cin >> a1;
    if (a1 == 679) c++;
    cout << endl;

    //10
    cout << "Решите уравнение: x^4 + x^3 - 7x^2 - x + 6 = 0" << endl;
    cout << "x1 = ";
    cin >> a1;
    cout << "x2 = ";
    cin >> a2;
    cout << "x3 = ";
    cin >> a3;
    cout << "x4 = ";
    cin >> a4;
    if ((a1 == -3 && a2 == -1 && a3 == 1 && a4 == 2) || (a1 == -3 && a2 == 1 && a3 == -1 && a4 == 2) || (a1 == -3 && a2 == 1 && a3 == 2 && a4 == -1) || \
        (a1 == -3 && a2 == 2 && a3 == 1 && a4 == -1) || (a1 == -3 && a2 == 2 && a3 == -1 && a4 == 1) || (a1 == -3 && a2 == -1 && a3 == 2 && a4 == 1) || \
        (a1 == -1 && a2 == -3 && a3 == 1 && a4 == 2) || (a1 == -1 && a2 == 1 && a3 == -3 && a4 == 2) || (a1 == -1 && a2 == 1 && a3 == 2 && a4 == -3) || \
        (a1 == -1 && a2 == 2 && a3 == 1 && a4 == -3) || (a1 == -1 && a2 == 2 && a3 == -3 && a4 == 1) || (a1 == -1 && a2 == -3 && a3 == 2 && a4 == 1) || \
        (a1 == 1 && a2 == -3 && a3 == -1 && a4 == 2) || (a1 == 1 && a2 == -3 && a3 == 2 && a4 == -1) || (a1 == 1 && a2 == 2 && a3 == -1 && a4 == -3) || \
        (a1 == 1 && a2 == 2 && a3 == -3 && a4 == -1) || (a1 == 1 && a2 == -1 && a3 == 2 && a4 == -3) || (a1 == 1 && a2 == -1 && a3 == -3 && a4 == 2) || \
        (a1 == 2 && a2 == -3 && a3 == -1 && a4 == 1) || (a1 == 2 && a2 == -3 && a3 == 1 && a4 == -1) || (a1 == 2 && a2 == 1 && a3 == -3 && a4 == -1) || \
        (a1 == 2 && a2 == 1 && a3 == -1 && a4 == -3) || (a1 == 2 && a2 == -1 && a3 == -3 && a4 == 1) || (a1 == 2 && a2 == -1 && a3 == 1 && a4 == -3)) c++;
    cout << endl;

    cout << "Количество правильно решенных заданий: " << c << endl;
}

void Eleventh_class()
{
    int c = 0;
    int a1, a2, a3, a4;
    int b1, b2, b3, b4, b5, b6, b7, b8;
    double c1, c2, c3, d1, d2;

    //1
    b1 = GetRandomNumber(2, 10);
    b2 = GetRandomNumber(3, 11);
    b3 = GetRandomNumber(4, 12);
    b4 = GetRandomNumber(3, 15);
    b5 = GetRandomNumber(1, 2);
    cout << "Найдите f'(" << b5 << "), если f(x) = " << b1 << "x^5 - " << b2 << "x^2 + " << b3 << " x + " << b4 << endl;
    cin >> a1;
    if (a1 == (b1 * 5 * pow(b5, 4)) - (b2 * 2 * b5) + b3) c++;
    cout << endl;

    //2
    cout << "Вычислите значение производной функции f(x) = tg(4x) в точке x0 = -pi/4" << endl;
    cin >> a1;
    if (a1 == 0) c++;
    cout << endl;

    //3
    cout << "Решите уравнение: (1 / 49)^-x = sqrt(1/7)" << endl << "x = ";
    cin >> a1;
    if (a1 == -0.25) c++;
    cout << endl;

    //4
    cout << "Решите уравнение: 3 * 5^(2x - 1) - 2 * 5^x = 5" << endl << "x = ";
    cin >> a1;
    if (a1 == 1) c++;
    cout << endl;

    //5
    cout << "Решите уравнение: sqrt(x + 6) = 0.25x + 0.25 " << endl << "x = " << endl;
    cin >> a1;
    if (a1 == 19) c++;
    cout << endl;

    //6
    cout << "Решите уравнение: sqrt(x + 5) = 0.5x + 1 " << endl << "x = " << endl;
    cin >> a1;
    if (a1 == 4) c++;
    cout << endl;

    //7
    cout << "Решите уравнение: (11^(x^2 - x) - 1) * sqrt(6x - 3) = 0 " << endl << "x1 = " << endl;
    cin >> a1;
    cout << "x2 = " << endl;
    cin >> a2;
    if ((a1 == 0.5 && a2 == 1) || (a1 == 1 && a2 == 0.5)) c++;
    cout << endl;

    //8
    cout << "Решите уравнение: tg((pi * x) / 4) = -1. В ответе запишите наибольший отрицательный корень";
    cin >> a1;
    if (a1 == -1) c++;
    cout << endl;

    //9
    cout << "В классе 26 учащихся, среди них два друга  — Андрей и Сергей.\
Учащихся случайным образом разбивают на 2 равные группы.\
Найдите вероятность того, что Андрей и Сергей окажутся в одной группе." << endl;
    cin >> a1;
    if (a1 == 0.48) c++;
    cout << endl;

    //10
    cout << "В понедельник акции компании подорожали на некоторое количество процентов, \
а во вторник подешевели на то же самое количество процентов.\
В результате они стали стоить на 4% дешевле, чем при открытии торгов в понедельник. \
На сколько процентов подорожали акции компании в понедельник?" << endl;
    cin >> a1;
    if (a1 == 20) c++;
    cout << endl;

    cout << "Количество правильно решенных заданий: " << c << endl;
}

int main()
{
    setlocale(LC_ALL, "Russian");

    int n;
    cout << "Введите номер класса, в котором вы обучаетесь (от 1 до 11): ";
    cin >> n;
    while (n < 1 || n > 11)
    {
        cout << endl << "Пожалуйста, введите номер класса: ";
        cin >> n;
    }
    cout << endl;
    switch (n)
    {
    case 1:
        First_class();
        break;
    case 2:
        Second_class();
        break;
    case 3:
        Third_class();
        break;
    case 4:
        Fourth_class();
        break;
    case 5:
        Fifth_class();
        break;
    case 6:
        Sixth_class();
        break;
    case 7:
        Seventh_class();
        break;
    case 8:
        Eight_class();
        break;
    case 9:
        Ninth_class();
        break;
    case 10:
        Tenth_class();
        break;
    case 11:
        Eleventh_class();
        break;
    }
    system("pause");
}